#include <DMXSerial.h>

//DMX Startadresse...
#define BASEADDR 505

//---Ab hier nichts ändern!
//Wird gemappt auf Arduino Port
#define STARTPORT 5
#define ENDPORT 12 //reserve 13 for signal status indicator
#define STATUSLED 13

void setup () {
  DMXSerial.init(DMXReceiver);
  
  // enable outputs
  for(int i=STARTPORT; i<=ENDPORT; i++) {
    pinMode(i, OUTPUT);
    DMXSerial.write(BASEADDR+i-STARTPORT, 0); //default to zero
    digitalWrite(i, HIGH);
  }
  
  pinMode(STATUSLED, OUTPUT);
  
  digitalWrite(STATUSLED, LOW); //Start with LOW until first DMX packet is received
}


void loop() {
  // Calculate how long no data packet was received
  unsigned long lastPacket = DMXSerial.noDataSince();
  
  if (lastPacket < 1000) {
    digitalWrite(STATUSLED, HIGH);
    int j=BASEADDR; //DMX-Address
    for(int i=STARTPORT; i<=ENDPORT; i++) {
      int r=DMXSerial.read(j);
      if(r>250) {
        digitalWrite(i, LOW);
      } else {
        digitalWrite(i, HIGH);
      }
      j++;
    }
  } else {
    for(int i=STARTPORT; i<=ENDPORT; i++) {
      digitalWrite(i, HIGH);
    }
    digitalWrite(STATUSLED, LOW);
  }
  delay(100);
}

// End.
